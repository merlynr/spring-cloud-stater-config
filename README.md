# spring-cloud-stater-config

#### 介绍
微服务配置

#### 安装教程

1. 刷新配置时，通过运行客户端的/refresh即可同步获得gitee最新配置信息
#### 使用说明

1.  可以配置WebHook来触发配置更新
#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request
