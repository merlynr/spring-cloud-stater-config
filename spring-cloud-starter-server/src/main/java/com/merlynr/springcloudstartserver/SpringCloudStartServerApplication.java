package com.merlynr.springcloudstartserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;

@EnableConfigServer
@SpringBootApplication
public class SpringCloudStartServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringCloudStartServerApplication.class, args);
    }

}
