package com.merlynr.springcloudstartclient;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringCloudStartClientApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringCloudStartClientApplication.class, args);
    }

}
